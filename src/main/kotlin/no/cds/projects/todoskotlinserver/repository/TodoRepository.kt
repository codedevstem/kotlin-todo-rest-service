package no.cds.projects.todoskotlinserver.repository

import no.cds.projects.todoskotlinserver.domain.Todo
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface TodoRepository : JpaRepository<Todo, UUID> {
}