package no.cds.projects.todoskotlinserver.exception

class PatchOperationNotPermittedException(message:String) : Throwable(message)