package no.cds.projects.todoskotlinserver.exception

class TodoNotFoundException(message:String) : Throwable(message)