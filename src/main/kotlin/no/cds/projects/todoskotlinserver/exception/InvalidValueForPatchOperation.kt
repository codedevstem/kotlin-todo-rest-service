package no.cds.projects.todoskotlinserver.exception

class InvalidValueForPatchOperation(message:String) : Throwable(message)