package no.cds.projects.todoskotlinserver.domain

import lombok.AllArgsConstructor
import java.util.*
import javax.persistence.*

@Entity
@Table(name="todo")
@AllArgsConstructor
data class Todo(@Id @GeneratedValue @Column(columnDefinition = "uuid")
                val id: UUID=UUID.randomUUID(),
                var description: String="",
                var completed: Boolean=false) {



}