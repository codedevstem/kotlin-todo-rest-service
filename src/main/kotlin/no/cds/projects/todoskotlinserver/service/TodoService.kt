package no.cds.projects.todoskotlinserver.service

import no.cds.projects.todoskotlinserver.domain.Todo
import no.cds.projects.todoskotlinserver.exception.InvalidValueForPatchOperation
import no.cds.projects.todoskotlinserver.exception.PatchOperationNotPermittedException
import no.cds.projects.todoskotlinserver.exception.TodoNotFoundException
import no.cds.projects.todoskotlinserver.repository.TodoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class TodoService @Autowired constructor(
        private val todoRepository: TodoRepository
) {
    fun findOne (id: UUID) : Todo {
       return todoRepository.findById(id).orElseThrow { createTodoNotFoundException(id) }
    }

    fun findAll () : List<Todo> {
        return todoRepository.findAll()
    }

    fun save (todo: Todo) {
        todoRepository.save(todo)
    }

    fun patch (updates: Map<String, Any>, id: UUID) {
        val todo: Todo = todoRepository.findById(id).orElseThrow { createTodoNotFoundException(id) }
        updates.forEach { (operator, value) ->
            when(operator) {
                "completed" -> {
                    if(value is Boolean) {
                        todo.completed = value
                    } else {
                        throw createIllegalValueException(operator, value)
                    }
                }
                "description" -> {
                    if(value is String) {
                        todo.description = value
                    } else {
                        throw createIllegalValueException(operator, value)
                    }
                }
                else -> throw PatchOperationNotPermittedException("Operation $operator not permitted")
            }
         }
        todoRepository.save(todo)
    }

    fun delete(id: UUID) {
        todoRepository.deleteById(id)
    }

    private fun createTodoNotFoundException(id: UUID) : Throwable {
       return TodoNotFoundException("Todo with id: $id could not be found")
    }

    private fun createIllegalValueException(operator: String, value: Any) : Throwable{
        return InvalidValueForPatchOperation("Value $value not permitted for operation $operator")
    }


}
