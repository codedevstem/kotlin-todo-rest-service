package no.cds.projects.todoskotlinserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TodosKotlinServerApplication

fun main(args: Array<String>) {
    runApplication<TodosKotlinServerApplication>(*args)
}
