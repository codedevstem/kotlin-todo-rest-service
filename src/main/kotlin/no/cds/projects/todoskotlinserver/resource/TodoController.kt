package no.cds.projects.todoskotlinserver.resource

import no.cds.projects.todoskotlinserver.domain.Todo
import no.cds.projects.todoskotlinserver.service.TodoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import java.util.*
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestMapping



@RestController
@RequestMapping("/todo")
class TodoController @Autowired constructor(
    private val todoService: TodoService

) {
    @GetMapping("{id}")
    fun one(@PathVariable id:UUID) : Todo {
        return todoService.findOne(id)
    }

    @GetMapping("/")
    fun all() : List<Todo> {
        return todoService.findAll()
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody todo: Todo) {
        todoService.save(todo)
    }

    @PatchMapping("{id}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun patch(
            @RequestBody updates: Map<String, Any>,
            @PathVariable("id") id: UUID): ResponseEntity<*> {

        todoService.patch(updates, id)
        return ResponseEntity.ok("resource updated")
    }
    @DeleteMapping("{id}")
    fun delete(@PathVariable("id") id: UUID): ResponseEntity<*> {
        todoService.delete(id)
        return ResponseEntity.ok("resource deleted")
    }
}